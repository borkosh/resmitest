using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.SqlServer;
using System.Data.SqlClient;

namespace ResmiTest.Models
{
     public class ApplicationDbContext : DbContext
    {
        public DbSet<Divination> Divinations { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options) { }
    }
}