using System;
namespace ResmiTest.Models
{
    public class Divination
    {
        public int Id { get; set; }

        public string Name { get; set; } 
        public string PhoneNumber { get; set; }
        public float Degree { get; set; }
        public DateTime DateAdded { get; set; }=DateTime.Now;
    }
}