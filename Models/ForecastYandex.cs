using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Collections.Generic;
using Newtonsoft.Json; 
using Newtonsoft.Json.Serialization;
namespace ResmiTest.Models
{
    public class ForecastInfo
    {
        public string Date { get; set; }      
        public int TempAvg { get; set; }  
        public string City { get; set; }   
    }
    public class ForecastYandex
    {
        [JsonProperty("now")]
        public long Now { get; set; }
        [JsonProperty("now_dt")]
        public string NowDt { get; set; }
        [JsonProperty("info")]
        public Info Info { get; set; }
        [JsonProperty("fact")]
        public Fact Fact { get; set; }
         [JsonProperty("forecasts")]
        public List<Forecast> Forecast { get; set; }
    }

    public class Info
    {
        [JsonProperty("tzinfo")]
        public Tzinfo Tzinfo { get; set; }     
    }
    public class Tzinfo
    {
        [JsonProperty("name")]
        public string Name { get; set; }     
    }

    public class Fact
    {
        [JsonProperty("temp")]
        public double Temp { get; set; }
     
    }

    [DataContract]
    public class Forecast
    {
         [JsonProperty("date")]
        public string Date { get; set; }
         [JsonProperty("date_ts")]
        public long DateTs { get; set; }
         [JsonProperty("parts")]

        public Part Parts { get; set; }
    }
 
    public class Part
    {
        [JsonProperty("day")]

        public Day Day { get; set; }
       
    }
    public class Day
    {
        [JsonProperty("temp_min")]
        public int TempMin { get; set; }

        [JsonProperty("temp_max")]
        public int TempMax { get; set; }

        [JsonProperty("temp_avg")]
        public int TempAvg { get; set; }
       
    }
   
}