using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Cors;
using System;
using System.Text.RegularExpressions; 
using System.Collections.Generic;
using System.Data;
using System.Net.Http;
using System.Net.Http.Headers;
using ResmiTest.Models;
using ResmiTest.Services;

namespace ResmiTest.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class DivinationController:ControllerBase
    {
        private readonly IDivinationService _divinationService;

        public DivinationController(IDivinationService divinationService)
        {
            _divinationService = divinationService;
        }

        [EnableCors("_myAllowSpecificOrigins")]
        [HttpPost("new")]
        public IActionResult AddDivination(Divination newDivination)
        {
            

            if (!Regex.Match(newDivination.Name, @"[а-яА-Я]$").Success) 
            { 
                ModelState.AddModelError("Name", "Недопустимое значение. Только киррилица");
            }

            if (!Regex.Match(newDivination.PhoneNumber, @"^\d{11}$").Success)
            { 
                ModelState.AddModelError("PhoneNumber", "Недопустимое значение. 11 цифр");
            }else{
                    var divi=_divinationService.GetDivinationsByNumber(newDivination.PhoneNumber);
                    if (divi.Data!=null) 
                    { 
                        ModelState.AddModelError("PhoneNumber", "Такой номер уже зарегистрирован!");
                    }
            }
            
            if ((newDivination.Degree >100) || (newDivination.Degree <-100))
            {
                ModelState.AddModelError("Degree", "Недопустимое значение. От -100 до 100");
            }

            
            // если есть лшибки - возвращаем ошибку 400
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            return Ok(_divinationService.AddDivination(newDivination));
        }
        
        [EnableCors("_myAllowSpecificOrigins")]
        [HttpPost("list")]
        public IActionResult Get(Divination newDivination)
        {
            return Ok(_divinationService.GetDivinations());
        }

        [EnableCors("_myAllowSpecificOrigins")]
        [HttpGet("forecast")]
        public IActionResult GetForecast()
        {
            return Ok(_divinationService.GetForecast());
        }

       
    }
}