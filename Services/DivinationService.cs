using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ResmiTest.Models;
using System.Text.Json;
using System.Net.Http;
using System.Threading.Tasks;
using System.Net.Http.Headers;
using Newtonsoft.Json; 
using Newtonsoft.Json.Serialization;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;

namespace ResmiTest.Services
{
    public class DivinationService: IDivinationService 
    {
        private readonly ApplicationDbContext _context;

        private static List<Divination> divinations = new List<Divination> {
            new Divination()
        };
        public DivinationService(ApplicationDbContext context)
        {
            _context = context;
        }

        public ServiceResponse<ForecastInfo> GetForecast()
        {
            ServiceResponse<ForecastInfo> serviceResponse = new ServiceResponse<ForecastInfo>();
            ForecastYandex forecastYandex = new ForecastYandex();
            ForecastInfo forecastInfo = new ForecastInfo();

            try
            {
                using (HttpClient httpClient = new HttpClient())
                    {
                        httpClient.DefaultRequestHeaders.Accept.Clear();                        
                        httpClient.DefaultRequestHeaders.Add("X-Yandex-API-Key", "492485e5-8dbf-4586-9819-ef700384ca0c");
                        var response = httpClient.GetAsync("https://api.weather.yandex.ru/v1/forecast?lat=43.2567&lon=76.9286&lang=ru_RU").Result;
                        
                        if (response.IsSuccessStatusCode)
                        {

                            var responseContent = response.Content; 
                            string responseString = responseContent.ReadAsStringAsync().Result;
                            forecastYandex = JsonConvert.DeserializeObject<ForecastYandex>(responseString);
                        
                            
                                            
                            forecastInfo.Date=forecastYandex.Forecast[1].Date;
                            forecastInfo.TempAvg=forecastYandex.Forecast[1].Parts.Day.TempAvg;
                            forecastInfo.City=forecastYandex.Info.Tzinfo.Name;
                        
                            serviceResponse.Data=forecastInfo;                                                               
                        }
                    }
            }
            catch (Exception ex)
            {
                serviceResponse.Success = false;
                serviceResponse.Message = ex.Message;
            }
            return serviceResponse;
        }

       public ServiceResponse<Divination> GetDivinationsByNumber(string number)
       {
           ServiceResponse<Divination> serviceResponse = new ServiceResponse<Divination>();
             try
            {
                Divination divination = _context.Divinations.FirstOrDefault(p => p.PhoneNumber==number);
                if (divination != null) serviceResponse.Data=divination;
            }
            catch (Exception ex)
            {
                serviceResponse.Success = false;
                serviceResponse.Message = ex.Message;
            }
            return serviceResponse;
       }
       public ServiceResponse<List<Divination>> GetDivinations()
        {
            ServiceResponse<List<Divination>> serviceResponse = new ServiceResponse<List<Divination>>();
             try
            {

                var forecastYesterday= GetForecast();
                //divinations = _context.Divinations.ToList();
                //SqlParameter param = new SqlParameter("@Degree", 11);
                var param =forecastYesterday.Data.TempAvg;
                var responseExecute=_context.Divinations.FromSqlRaw("select [Id],[Name],[PhoneNumber],[Degree],[DateAdded] from ("+
" SELECT *,case WHEN {0}<[Degree] then [Degree]-{0} when {0}=[Degree] then [Degree] -{0}  else {0}-[Degree] END AS Diff"+
" FROM [Divinations] ) f order by Diff,[DateAdded],[Degree] ", param).ToList();

                serviceResponse.Data = responseExecute;

            }
            catch (Exception ex)
            {
                serviceResponse.Success = false;
                serviceResponse.Message = ex.Message;
            }
            return serviceResponse;
        }

        public ServiceResponse<List<Divination>> AddDivination(Divination newDivination)
        {
            ServiceResponse<List<Divination>> serviceResponse = new ServiceResponse<List<Divination>>();
            try
            {
                    _context.Divinations.Add(newDivination);
                    _context.SaveChanges();
                    divinations = _context.Divinations.ToList();
                    serviceResponse.Data = divinations;
            }
            catch (Exception ex)
            {
                serviceResponse.Success = false;
                serviceResponse.Message = ex.Message;
            }
            return serviceResponse;
        }
        
    }
}