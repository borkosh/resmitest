using ResmiTest.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json; 
using Newtonsoft.Json.Serialization;

namespace ResmiTest.Services
{
    public interface IDivinationService
    {
        ServiceResponse<List<Divination>> AddDivination(Divination newDivination);
        ServiceResponse<List<Divination>> GetDivinations();
        ServiceResponse<Divination> GetDivinationsByNumber(string number);
        ServiceResponse<ForecastInfo> GetForecast();    
    }
}